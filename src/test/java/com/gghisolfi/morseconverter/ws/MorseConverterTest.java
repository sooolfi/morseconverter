package com.gghisolfi.morseconverter.ws;

import com.gghisolfi.morseconverter.util.MorseUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

/**
 *
 * @author guido
 */
public class MorseConverterTest {

    @Rule
    public ExpectedException expect = ExpectedException.none();

    /**
     * Test of convert2Morse method, of class MorseConverter.
     *
     * @throws org.json.JSONException
     */
    @Test
    public void testConvert2MorseNulo() throws JSONException {
        MorseConverter converter = new MorseConverter();
        String res = converter.convert2Morse(null);
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_PARAMETROS, resJson.get("response"));
    }

    @Test
    public void testConvert2MorseParametroIncorrecto() throws JSONException {
        MorseConverter converter = new MorseConverter();
        JSONObject json = new JSONObject();
        json.put("texto", "incorrecto");
        String res = converter.convert2Morse(json.toString());
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_PARAMETROS, resJson.get("response"));
    }

    @Test
    public void testConvert2MorseValoresIncorrectos() throws JSONException {
        MorseConverter converter = new MorseConverter();
        JSONObject json = new JSONObject();
        json.put("text", "HOLA M^undo");
        String res = converter.convert2Morse(json.toString());
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_CARACTERES, resJson.get("response"));
    }

    @Test
    public void testConvert2MorseCorrecto() throws JSONException {
        MorseConverter converter = new MorseConverter();
        JSONObject json = new JSONObject();
        json.put("text", "HOLA MUNDO");
        String res = converter.convert2Morse(json.toString());
        JSONObject resJson = new JSONObject(res);
        assertEquals("200", resJson.get("code"));
    }
}
