package com.gghisolfi.morseconverter.ws;

import com.gghisolfi.morseconverter.util.MorseUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author guido
 */
public class TextConverterTest {

    @Test
    public void testConvert2TextNulo() throws JSONException {
        TextConverter converter = new TextConverter();
        String res = converter.convert2Text(null);
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_PARAMETROS, resJson.get("response"));
    }

    @Test
    public void testConvert2TextParametroIncorrecto() throws JSONException {
        TextConverter converter = new TextConverter();
        JSONObject json = new JSONObject();
        json.put("texto", "incorrecto");
        String res = converter.convert2Text(json.toString());
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_PARAMETROS, resJson.get("response"));
    }

    @Test
    public void testConvert2TextValoresIncorrectos() throws JSONException {
        TextConverter converter = new TextConverter();
        JSONObject json = new JSONObject();
        json.put("text", ".. -.. asd");
        String res = converter.convert2Text(json.toString());
        JSONObject resJson = new JSONObject(res);
        assertEquals("400", resJson.get("code"));
        assertEquals(MorseUtils.ERROR_CARACTERES, resJson.get("response"));
    }
    
    
    @Test
    public void testConvert2TextCorrecto() throws JSONException {
        TextConverter converter = new TextConverter();
        JSONObject json = new JSONObject();
        json.put("text", ".. -.. /");
        String res = converter.convert2Text(json.toString());
        JSONObject resJson = new JSONObject(res);       
        
        assertEquals("200", resJson.get("code"));
    }

}
