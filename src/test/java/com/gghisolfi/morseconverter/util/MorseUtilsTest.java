package com.gghisolfi.morseconverter.util;

import org.junit.Test;
import static org.junit.Assert.*;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author guido
 */
public class MorseUtilsTest {

    private static final String msjHuman = "Hola Mundo";
    private static final String msjMorse = ".... --- .-.. .- / -- ..- -. -.. ---";

    @Test
    public void testTranslate2MorseEmptyAndNull() {
        assertEquals("", MorseUtils.translate2Morse(null));
        assertEquals("", MorseUtils.translate2Morse(""));
    }

    @Test
    public void testTranslate2TextEmptyAndNull() {
        assertEquals("", MorseUtils.translate2Human(null));
        assertEquals("", MorseUtils.translate2Human(""));
    }

    /**
     * Test of translate2Morse method, of class MorseUtils.
     */
    @Test
    public void testTranslate2Morse() {
        String morse = MorseUtils.translate2Morse(msjHuman);
        assertEquals(msjMorse, morse);
        System.out.println(morse);
        String randomString = RandomStringUtils.randomAlphabetic(100).toLowerCase();
        assertEquals(randomString, MorseUtils.translate2Human(MorseUtils.translate2Morse(randomString)));

    }

    /**
     * Test of translate2Human method, of class MorseUtils.
     */
    @Test
    public void testTranslate2Human() {
        String human = MorseUtils.translate2Human(msjMorse);
        assertEquals(msjHuman.toLowerCase(), human);
        System.out.println(human);
    }

    @Test
    public void testBinaryTOMorse() {

        String test = "00000000110110110011100000"
                + "111111000111111001111110000000"
                + "111011111111011101110000000"
                + "1100011111100000"
                + "111111001111110000000"
                + "110000"
                + "1101111111101110111000000"
                + "11011100000000000";
        
        String test2 = "110011100110011100000111111001111100011111101111111";
        String test3 = "11111110000000011111111110000000000111111111000000000111111111000000000000000000000011111111111111111111111110000000";        
        
        String testFullStop = "1100111001100111000001111110011111000"
                + "110011111001100111110011001111100110011111" //fullstop
                + "001111111101111111";
        
        
        
        String test4 ="11 00 111 00 11 00 111 00000 111111 00 11111 000 111111 0 1111111 ";
        
        System.out.println("Expected: .... --- .-.. .- -- . .-.. ..");               
        System.out.println("Result: "+MorseUtils.decodeBits2Morse(test));        
        
        System.out.println("Expected: .... ----");
        System.out.println("Result: "+MorseUtils.decodeBits2Morse(test2));        
        
        System.out.println("Expected: .... -");
        System.out.println("Result: "+MorseUtils.decodeBits2Morse(test3));        
        
        System.out.println("Expected: .... --");
        System.out.println("Result: "+MorseUtils.decodeBits2Morse(testFullStop));
        
        System.out.println("Expected: .... ----");
        System.out.println("Result: "+MorseUtils.decodeBits2Morse(test4));        
        
    }

    @Test
    public void testGetSequence() {
        String testString = "0000001111111";
        assertEquals(6, MorseUtils.getSequence(testString, false));
        String testString2 = "11000000";
        assertEquals(2, MorseUtils.getSequence(testString2, true));        
        assertEquals(0, MorseUtils.getSequence(null, true));
        assertEquals(0, MorseUtils.getSequence(null, false));
    }
    
    
    
    @Test
    public void testgetMinimunPulseDuration() {
        String testString = "0000001111111001110000110000000111000111";
        assertEquals(2, MorseUtils.getMinimunPulseDuration(testString));
        String testString2 = "11100000000000011111110000111";
        assertEquals(3, MorseUtils.getMinimunPulseDuration(testString2));        
        assertEquals(0, MorseUtils.getMinimunPulseDuration(null));        
    }
    
    
    
    

}
