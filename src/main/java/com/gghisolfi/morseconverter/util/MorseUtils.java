package com.gghisolfi.morseconverter.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author guido
 */
public class MorseUtils {

    public static final String ERROR_PARAMETROS = "Debe realizar la llamada con el parametro text.";
    public static final String ERROR_CARACTERES = "El texto ingresado contiene caracteres incorrectos.";

    private static final String[] morsePatterns = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
        "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
        "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-",
        "-.--", "--..", ".----", "..---", "...--", "....-", ".....",
        "-....", "--...", "---..", "----.", "-----", "/"};

    private static final String fullStop = ".-.-.-";

    private static final String[] humanPatterns = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
        "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
        "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8",
        "9", "0", " "};

    public static String translate2Morse(String msj) {

        if (msj == null || msj.isEmpty()) {
            return "";
        }

        String[] humanSplit = msj.split("");
        String resultado = "";

        for (String code : humanSplit) {
            resultado += " " + getMorse(code);
        }

        return resultado.substring(1, resultado.length());

    }

    public static String translate2Human(String morseCode) {
        if (morseCode == null || morseCode.isEmpty()) {
            return "";
        }

        String[] morseSplit = morseCode.split(" ");
        String resultado = "";

        for (String code : morseSplit) {
            resultado += getHuman(code);
        }

        return resultado;

    }

    private static String getHuman(String code) {
        for (int i = 0; i < morsePatterns.length; i++) {
            if (code.equals(morsePatterns[i])) {
                return humanPatterns[i];
            }
        }
        return "";
    }

    private static String getMorse(String human) {

        for (int i = 0; i < humanPatterns.length; i++) {
            if (human.toLowerCase().equals(humanPatterns[i])) {
                return morsePatterns[i];
            }
        }
        return "";
    }

    
    /**
     * Se considera que la representación es consistente durante
       todo el mensaje. Es decir, si un operador es lento o rápido, seguirá siendo lento o rápido a
       lo largo del envío de ese mensaje.
       * 
       * Teniendo en cuenta el enunciado, en la misma cadena de bits no puede represetarse un punto por ejemplo
       * con 2 bits y con 8 bits, ya que puede sufrir solo pequeñas variaciones.
       * 
     * @param bitsMessage
     * @return 
     */
    
    
    public static String decodeBits2Morse(String bitsMessage) {

        String morseResult = "";
        bitsMessage = bitsMessage.replaceAll(" ", "");
        String startPulseMsj = bitsMessage.substring(bitsMessage.indexOf("1"), bitsMessage.length());
        
        //Busca la durancion minima que representa un punto en la cadena de bits
        int pulseInitial = getMinimunPulseDuration(startPulseMsj);

        boolean banderaBusqueda = true;

        while (!startPulseMsj.isEmpty()) {

            //Obtiene la secuencia de numeros consecutivos de bits iguales
            int dimensionSeq = getSequence(startPulseMsj, banderaBusqueda);

            String seqMsj = startPulseMsj.substring(0, dimensionSeq);
            String morsePhrase = convertChain(seqMsj, pulseInitial);
            morseResult += morsePhrase;


            //Si encuentra el patron retorna el mensaje..
            if (morseResult.contains(fullStop)) {                
                return morseResult.substring(0, morseResult.length() - fullStop.length());

            }

            startPulseMsj = startPulseMsj.substring(dimensionSeq, startPulseMsj.length());
            banderaBusqueda = !banderaBusqueda;
        }

        return morseResult;

    }

    /**
     * Obtiene el numero de bits continuos, bit en verdadero retorna la cantidad
     * de unos, bit en falso retorna la cantidad de ceros consecutivos
     *
     * @param phrase
     * @param bit
     * @return
     */
    public static int getSequence(String phrase, boolean bit) {

        if (phrase == null) {
            return 0;
        }

        int pos = 0;
        for (int i = 0; i < phrase.length(); i++) {
            if (bit && phrase.charAt(i) == '1') {
                pos++;
            } else {
                if (!bit && phrase.charAt(i) == '0') {
                    pos++;
                } else {
                    return pos;
                }
            }

        }
        return pos;
    }

    /**
     * Obtengo la minima secuencia de unos, para determinar la duración minima
     * de un pulso, para luego aproximar
     *
     * @param msj
     * @return
     */
    public static int getMinimunPulseDuration(String msj) {
        if (msj == null) {
            return 0;
        }
        String[] args = msj.split("0");
        List<String> wordList = Arrays.asList(args);
        ArrayList<String> res = new ArrayList<>(wordList);
        res.removeIf(item -> item == null || "".equals(item));
        Collections.sort(res);
        return res.get(0).length();
    }

    /**
     * Convierte una cadena en su correspondiente notacion en morse La duracion del pulso aproximada 
     * es enviada para aproximar su conversion.
     *
     * @param chain
     * @param lastPulseDuration
     * @return
     */
    public static String convertChain(String chain, int lastPulseDuration) {

        // Estandar codigo morse.
        // .  1 pulsos
        // -  3 pulsos
        // Espacio entre palabras 7 pulsos
        //Espacio entre letras 3 Pulsos
        //Espacio entre señales misma letra 1 pulso
        int size = chain.length();

        if (chain.contains("1")) {

            //Calculamos la distancia al punto y a la barra
            int distBarra = lastPulseDuration * 3 - size;
            int distPunto = lastPulseDuration - size;

            
            if (Math.abs(distBarra) < Math.abs(distPunto)) {
                return "-";
            } else {
                return ".";
            }

        } else {
            int distEspacioPalabras = lastPulseDuration * 7 - size;
            int distEspacioLetra = lastPulseDuration * 3 - size;
            int distEspacioMismaLetra = lastPulseDuration - size;

            if (Math.abs(distEspacioMismaLetra) < Math.abs(distEspacioLetra)
                    && Math.abs(distEspacioMismaLetra) < Math.abs(distEspacioPalabras)) {
                return ""; //separacion entre misma letra
            } else {
                if (Math.abs(distEspacioPalabras) < Math.abs(distEspacioLetra)) {
                    return "/"; //separacion entre palabras
                } else { 
                    return " "; //Separacion entre letras
                }
            }

        }

    }

}
