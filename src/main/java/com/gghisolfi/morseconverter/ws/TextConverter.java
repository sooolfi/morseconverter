package com.gghisolfi.morseconverter.ws;

import com.gghisolfi.morseconverter.util.MorseUtils;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * REST Web Service
 *
 * @author guido
 */
@Path("2text")
public class TextConverter {

    /**
     * Creates a new instance of TextConverter
     */
    public TextConverter() {
    }

    /**
     * Retrieves representation of an instance of morseConverter.MorseConverter
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getXml() {
        return "{\"message\":\"Activo\"}\n";
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String convert2Text(String json) {

        if (json == null) {
            return "{\"code\":\"400\","
                    + "\"response\":\"" + MorseUtils.ERROR_PARAMETROS + "\"}\n";
        }

        try {
            JSONObject obj = new JSONObject(json);
            String text = obj.getString("text");

            boolean validText = text.matches("[-,., ,/]*");
            if (!validText) {
                return "{\"code\":\"400\","
                        + "\"response\":\"" + MorseUtils.ERROR_CARACTERES + "\"}\n";
            }
            System.out.println(validText);
            String conversion = MorseUtils.translate2Human(text);

            return "{\"code\":\"200\","
                    + "\"response\":\"" + conversion + "\"}\n";
        } catch (JSONException e) {
            return "{\"code\":\"400\","
                    + "\"response\":\"" + MorseUtils.ERROR_PARAMETROS + "\"}\n";
        }

    }
}
